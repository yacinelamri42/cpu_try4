#ifndef STACK__H
#define STACK__H

#define PUSH(stack, sp, data) (stack)[(sp)++] = (data)

#define POP(stack, sp) (stack)[--(sp)]

#endif
