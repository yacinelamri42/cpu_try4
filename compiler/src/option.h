#ifndef OPTION__H
#define OPTION__H

#ifndef BASIC_ENUM
#define BASIC_ENUM
typedef enum {
	OPTION_ENUM_NONE,
	OPTION_ENUM_SOME,
} OptionEnum;
#endif

#define OPTION_DEFINE(option_name, type) typedef struct {OptionEnum option; type data;} option_name;

#define OPTION_WRAP(option_name, d) (option_name) {.option = OPTION_ENUM_SOME, .data = d}

#define OPTION_UNWRAP(option, varname) typeof (option.data) varname; if(option.option) {varname = option.data;} else {PANIC("there is nothing to unwarp\n");}

#endif
