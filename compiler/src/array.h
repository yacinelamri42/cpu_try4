#ifndef ARRAY__H
#define ARRAY__H

#include <stddef.h>

#define ARRAY_DEFINE(type, array_name) typedef struct {type* arr; size_t len;} array_name

#define ARRAY_FROM_CARRAY(array_name, carray) (array_name) {.arr = carray, .len = sizeof(array)/sizeof(*array)}

#define ARRAY_CREATE(array_name, p, l) (array_name) {.arr = p, .len = l}

#endif
