#include "token.h"
#include "list.h"
#include "str.h"
#include <stdio.h>

char* token_names[] = {
	[TOKENTYPE_NONE] = "TOKENTYPE_NONE",
	[TOKENTYPE_PLUS] = "TOKENTYPE_PLUS",
	[TOKENTYPE_PLUS_EQUAL] = "TOKENTYPE_PLUS_EQUAL",
	[TOKENTYPE_MINUS] = "TOKENTYPE_MINUS",
	[TOKENTYPE_MINUS_EQUAL] = "TOKENTYPE_MINUS_EQUAL",
	[TOKENTYPE_STAR] = "TOKENTYPE_STAR",
	[TOKENTYPE_STAR_EQUAL] = "TOKENTYPE_STAR_EQUAL",
	[TOKENTYPE_SLASH] = "TOKENTYPE_SLASH",
	[TOKENTYPE_SLASH_EQUAL] = "TOKENTYPE_SLASH_EQUAL",
	[TOKENTYPE_LEFT_PARENTHESIS] = "TOKENTYPE_LEFT_PARENTHESIS",
	[TOKENTYPE_RIGHT_PARENTHESIS] = "TOKENTYPE_RIGHT_PARENTHESIS",
	[TOKENTYPE_LEFT_BRACKET] = "TOKENTYPE_LEFT_BRACKET",
	[TOKENTYPE_RIGHT_BRACKET] = "TOKENTYPE_RIGHT_BRACKET",
	[TOKENTYPE_LEFT_BRACE] = "TOKENTYPE_LEFT_BRACE",
	[TOKENTYPE_RIGHT_BRACE] = "TOKENTYPE_RIGHT_BRACE",
	[TOKENTYPE_EQUAL] = "TOKENTYPE_EQUAL",
	[TOKENTYPE_EQUAL_EQUAL] = "TOKENTYPE_EQUAL_EQUAL",
	[TOKENYPE_BANG_EQUAL] = "TOKENYPE_BANG_EQUAL",
	[TOKENTYPE_BIGGER] = "TOKENTYPE_BIGGER",
	[TOKENTYPE_BIGGER_EQUAL] = "TOKENTYPE_BIGGER_EQUAL",
	[TOKENTYPE_SMALLER] = "TOKENTYPE_SMALLER",
	[TOKENTYPE_SMALLER_EQUAL] = "TOKENTYPE_SMALLER_EQUAL",
	[TOKENTYPE_IDENTIFIER] = "TOKENTYPE_IDENTIFIER",
	[TOKENTYPE_STRING_LITTERAL] = "TOKENTYPE_STRING_LITTERAL",
	[TOKENTYPE_INTEGER] = "TOKENTYPE_INTEGER",
	// [// TOKENTYPE_DECIMAL] = "TOKENTYPE_DECIMAL",
	[TOKENTYPE_IF] = "TOKENTYPE_IF",
	[TOKENTYPE_ELSE] = "TOKENTYPE_ELSE",
	[TOKENTYPE_ELIF] = "TOKENTYPE_ELIF",
	[TOPENTYPE_FN] = "TOPENTYPE_FN",
	[TOKENTYPE_STRUCT] = "TOKENTYPE_STRUCT",
	[TOKENTYPE_FOR] = "TOKENTYPE_FOR",
	[TOKENTYPE_WHILE] = "TOKENTYPE_WHILE",
	[TOKENTYPE_INT] = "TOKENTYPE_INT",
	// TOKENTYPE_FLOAT,
	// TOKENTYPE_DOUBLE,
	[TOKENTYPE_CHAR] = "TOKENTYPE_CHAR",
	[TOKENTYPE_EOF] = "TOKENTYPE_EOF",
};

Error token_array_generate(TokenList* tokenList, Str code) {
	size_t i = 0;
	size_t line_num = 0;
	for (i=0; i<code.len; i++) {
		char c = code.str[i];
		char c1 = (i<code.len-1) ? code.str[i+1] : 0;
		char c2 = (i<code.len-2) ? code.str[i+2] : 0;
		// char c3 = (i<code.len-3) ? code.str[i+3] : 0;
		Token token = {
			.line_num = line_num,
			.byte_num = i,
		};
		if (c == '\n') {
			line_num++;
		}
		if (LETTER_IS_SPACE(c)) {
			continue;
		} else if (c == '+' && c1 != '=') {
			token.token_type = TOKENTYPE_PLUS;
		} else if (c == '+' && c1 == '=') {
			token.token_type = TOKENTYPE_PLUS_EQUAL;
			i++;
		} else if (c == '-' && c1 != '=') {
			token.token_type = TOKENTYPE_MINUS;
		} else if (c == '-' && c1 == '=') {
			token.token_type = TOKENTYPE_MINUS_EQUAL;
			i++;
		} else if (c == '*' && c1 != '=') {
			token.token_type = TOKENTYPE_STAR;
		} else if (c == '*' && c1 == '=') {
			token.token_type = TOKENTYPE_STAR_EQUAL;
			i++;
		} else if (c == '/' && c1 != '=') {
			token.token_type = TOKENTYPE_SLASH;
		} else if (c == '/' && c1 == '=') {
			token.token_type = TOKENTYPE_SLASH_EQUAL;
			i++;
		} else if (c == '(') {
			token.token_type = TOKENTYPE_LEFT_PARENTHESIS;
		} else if (c == ')') {
			token.token_type = TOKENTYPE_RIGHT_PARENTHESIS;
		} else if (c == '[') {
			token.token_type = TOKENTYPE_LEFT_BRACKET;
		} else if (c == ']') {
			token.token_type = TOKENTYPE_RIGHT_BRACKET;
		} else if (c == '{') {
			token.token_type = TOKENTYPE_LEFT_BRACE;
		} else if (c == '}') {
			token.token_type = TOKENTYPE_RIGHT_BRACE;
		} else if (c == '=' && c1 != '=') {
			token.token_type = TOKENTYPE_EQUAL;
		} else if (c == '=' && c1 == '=') {
			token.token_type = TOKENTYPE_EQUAL_EQUAL;
			i++;
		} else if (c == '<' && c1 != '=') {
			token.token_type = TOKENTYPE_SMALLER;
		} else if (c == '<' && c1 == '=') {
			token.token_type = TOKENTYPE_SMALLER_EQUAL;
			i++;
		} else if (c == '>' && c1 != '=') {
			token.token_type = TOKENTYPE_BIGGER;
		} else if (c == '>' && c1 == '=') {
			token.token_type = TOKENTYPE_BIGGER_EQUAL;
			i++;
		} else if (c == '\'') {
			token.token_type = TOKENTYPE_CHAR;
			token.token_value.token_char = c1;
			i++;
			i++;
			if (c1 == '\\') {
				token.token_value.token_char = c2;
				i++;
			}
			if (c2 != '\'') {
				return (Error) {
					.type=TOKENERROR_INVALID_CHAR, 
					.byte_num =  i, 
					.line_num = line_num
				};
			}
		} else if (c == '\"') {
			token.token_type = TOKENTYPE_STRING_LITTERAL;
			Str s = {
				.len = 0,
				.str = code.str+i+1
			};
			i++;
			for (; i<code.len && code.str[i] != '\"' && code.str[i] != '\n' && code.str[i-1] != '\\'; i++, s.len++);
			token.token_value.token_string = s;
		} else if (LETTER_IS_NUM(c)) {
			token.token_type = TOKENTYPE_INTEGER;
			Str s = {
				.str = code.str+i,
				.len = 0
			};
			uint8_t base = 10;
			if (s.str[0] == '0') {
				switch (s.str[1]) {
					case 'x':
					case 'X':
						base = 16;
						s.str+=2;
						i+=2;
					break;
					case 'o':
					case 'O':
						base = 8;
						s.str+=2;
						i+=2;
					break;
					case 'b':
					case 'B':
						base = 2;
						s.str+=2;
						i+=2;
					break;
				}
			}
			for (; i<code.len && LETTER_IS_NUM(code.str[i]); s.len++, i++);
			uint32_t num = 0;
			if(!str_to_uint32_t_with_error(s, &num, base, 0)) {
				wrap_print(s);
				return (Error) {
					.type=TOKENERROR_INVALID_CHAR, 
					.byte_num =  i, 
					.line_num = line_num
				};
			}
			token.token_value.token_int = (int)num;
		} else if (LETTER_IS_ALPHA(c) || c == '_') {
			Str s = {
				.str = code.str+i,
				.len = 0
			};
			for (; i<code.len && LETTER_IS_ALPHANUM(code.str[i]); s.len++, i++);
			token.token_value.token_string = s;
			if (!str_compare(s, STR_FROM_CSTRING_ARR("if"), false)) {
				token.token_type = TOKENTYPE_IF;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("for"), false)) {
				token.token_type = TOKENTYPE_FOR;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("elif"), false)) {
				token.token_type = TOKENTYPE_ELIF;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("else"), false)) {
				token.token_type = TOKENTYPE_ELSE;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("while"), false)) {
				token.token_type = TOKENTYPE_WHILE;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("struct"), false)) {
				token.token_type = TOKENTYPE_STRUCT;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("char"), false)) {
				token.token_type = TOKENTYPE_CHAR;
			} else if (!str_compare(s, STR_FROM_CSTRING_ARR("int"), false)) {
				token.token_type = TOKENTYPE_INT;
			} else {
				token.token_type = TOKENTYPE_IDENTIFIER;
			}
		} else {
			printf("??\n");
			return (Error) {
				.type=TOKENERROR_INVALID_CHAR, 
				.byte_num =  i, 
				.line_num = line_num
			};
		}
		LIST_PUSH(*tokenList, token);
	}
	Token eof = {
		.byte_num = i,
		.line_num = line_num,
		.token_type = TOKENTYPE_EOF,
	};
	LIST_PUSH(*tokenList, eof);
	return (Error) {
		.type = TOKENERROR_NONE
	};
}

void token_print(Token t) {
	printf("Token:\n");
	printf("\ttoken type: %s\n", token_names[t.token_type]);
	printf("\ttoken content: ");
	if (t.token_type == TOKENTYPE_CHAR) {
		printf("\'%c\'\n", t.token_value.token_char);
	} else if (t.token_type == TOKENTYPE_INTEGER) {
		printf("%d\n", t.token_value.token_int);
	} else {
		wrap_print(t.token_value.token_string);
	}
}
