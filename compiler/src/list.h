#ifndef LIST__H
#define LIST__H

#include <stdlib.h>
#include <stdio.h>
#include "stack.h"
#include "panic.h"

#define LIST_DEFAULT_CAPACITY 16

#define LIST_DEFINE(type, list_name) typedef struct {type* data; size_t num_data; size_t capacity;} list_name;

#define LIST_CREATE_WITH_CUSTOM_CAPACITY(list_name, type, cap) (list_name) {.data=calloc((cap), sizeof(type)) ,.num_data=0, .capacity=(cap)}

#define LIST_CREATE(list_name, type) LIST_CREATE_WITH_CUSTOM_CAPACITY(list_name, type, LIST_DEFAULT_CAPACITY)

#define LIST_PUSH(list, d) \
	if((list).num_data == (list).capacity) { \
		typeof ((list).data) temp = reallocarray((list).data, (list).capacity*2, sizeof(*(list).data)); \
		if(!(list).data) PANIC("allocation error");\
		(list).data = temp; \
		(list).capacity*=2;\
	} if((list).data) PUSH((list).data, (list).num_data, d) \

#define LIST_POP(list) ((list).num_data) ? POP((list).data, (list).num_data) : 0

#define LIST_DESTROY(list) free((list).data); (list).data = 0; (list).num_data = 0; (list).capacity = 0

#endif
