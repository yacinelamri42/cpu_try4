#ifndef BTREE__H
#define BTREE__H

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "option.h"

#define BTREE_DEFINE(node_name, type) \
	typedef struct {type data; size_t left; size_t right; size_t parent; size_t current_index;} node_name; \
	OPTION_DEFINE(node_name##Option, node_name);\
	typedef struct {node_name##Option* data; size_t depth;} node_name##List;

#define BTREE_CREATE(node_name) (node_name##List) {.data = calloc(1, sizeof(node_name)), .depth = 0}

#define BTREE_DESTROY(node_list) free((node_list).data); (node_list).depth = 0; (node_list).data = 0;

#define BTREE_APPEND(node_name, node_list, node, parent_ptr, isLeft) \
	if(((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0) < (1 << (node_list).depth) && (((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0) >= (1 << ((node_list).depth-1)))) { \
		typeof ((node_list).data) temp = reallocarray((node_list).data, 1 << (++(node_list).depth), sizeof(*temp));\
		if(!temp) PANIC("could not allocate memory\n");\
		(node_list).data = temp;\
	}\
	(node_list).data[2*(((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0)) + ((isLeft) ? 0 : 1)] = OPTION_WRAP(node_name##Option, node);\
	(node).current_index = 2*(((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0)) + ((isLeft) ? 0 : 1); \
	if(parent_ptr) {\
		if(isLeft) {((typeof (&((node_list).data->data)))parent_ptr)->left = 2*(((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0)) + ((isLeft) ? 0 : 1);}\
		else {((typeof (&((node_list).data->data)))parent_ptr)->left = 2*(((parent_ptr) ? ((typeof (&((node_list).data->data)))parent_ptr)->current_index : 0)) + ((isLeft) ? 0 : 1);}\
	}

#endif
