#ifndef QUEUE__H
#define QUEUE__H

#define QUEUE_ENQUEUE(queue, queue_start, queue_end, queue_max_len, x) queue[(queue_end)] = (x); (queue_end) = (((queue_end) + 1) % (queue_max_len))

#define QUEUE_DEQUEUE(queue, queue_start, queue_end, queue_max_len) queue[(queue_end)]; (queue_end) = (((queue_end) + 1) % (queue_max_len))

#define QUEUE_SIZE(queue, queue_start, queue_end, queue_max_len) ((((queue_max_len) - (queue_start)) + (queue_end)) % (queue_max_len))

#define QUEUE_IS_EMPTY(queue, queue_start, queue_end, queue_max_len) ((queue_end) == (queue_start))

#endif
