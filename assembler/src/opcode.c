#include "opcode.h"
#include "str.h"
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef struct {
	Str instruction_str;
	uint8_t opcode;
	uint8_t enable_addressing_mode: 1;
	uint8_t argument_count: 2;
} Instruction;

typedef struct {
	Str tag_name;
	uint32_t addr;
} Tag;

uint64_t num_tags = 1;
Tag tags[8192] = {
	(Tag) {.tag_name = STR_FROM_CSTRING_ARR("$"), .addr = -2}
};

void tag_print(Tag tag) {
	printf("tag name: ");
	wrap_print(tag.tag_name);
	printf("\n tag number: %d\n", tag.addr);
}

typedef enum {
	ERROR_NONE,
	ERROR_TOO_MANY_ARGS,
	ERROR_INSTRUCTION_NOT_FOUND,
	ERROR_TOO_LITTLE_ARGS,
	ERROR_UNKNOWN_SYMBOL,
	ERROR_INVALID_ADDRESSING_MODE,
	ERROR_INVALID_NUMBER,
	ERROR_TAG_NOT_FOUND,
	ERROR_END,
} ErrorType;

char* error_type_strings[] = {
	[ERROR_NONE] = "no error",
	[ERROR_END] = "no error",
	[ERROR_INVALID_ADDRESSING_MODE] = "invalid addressing mode error",
	[ERROR_TOO_LITTLE_ARGS] = "too little args error",
	[ERROR_UNKNOWN_SYMBOL] = "unknown symbol error",
	[ERROR_INVALID_NUMBER] = "invalid number error",
	[ERROR_TOO_MANY_ARGS] = "too many args error",
	[ERROR_TAG_NOT_FOUND] = "tag not found",
	[ERROR_INSTRUCTION_NOT_FOUND] = "instruction not found",
};

typedef struct {
	ErrorType error;
	char* position;
} Error;

Instruction instructions[] = {
	(Instruction){STR_FROM_CSTRING_ARR("NOP"),       0x00, 0, 0},
	(Instruction){STR_FROM_CSTRING_ARR("MOV"),       0x10, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("STORE"),     0x18, 0, 2},
	(Instruction){STR_FROM_CSTRING_ARR("ADD"),       0x20, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("SUB"),       0x30, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("AND"),       0x40, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("OR"),        0x50, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("NOT"),       0x60, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("SHIFTL"),    0x70, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("SHIFTR"),    0x80, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("JMP"),       0x90, 1, 1},
	(Instruction){STR_FROM_CSTRING_ARR("JZ"),        0xA0, 1, 1},
	(Instruction){STR_FROM_CSTRING_ARR("JC"),        0xB0, 1, 1},
	(Instruction){STR_FROM_CSTRING_ARR("IN"),        0xC0, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("OUT"),       0xD0, 1, 2},
	(Instruction){STR_FROM_CSTRING_ARR("INT"),       0xE0, 1, 1},
	(Instruction){STR_FROM_CSTRING_ARR("IRET"),      0xE6, 0, 0},
	(Instruction){STR_FROM_CSTRING_ARR("PAUSE_INT"), 0xE7, 0, 0},
	(Instruction){STR_FROM_CSTRING_ARR("RESUME_INT"),0xE8, 0, 0},
};

Instruction* find_instruction(Str command) {
	for (int i=0; i<sizeof(instructions)/sizeof(instructions[0]); i++) {
		if (!str_compare(command, instructions[i].instruction_str, true)) {
			return instructions+i;
		}
	}
	return 0;
}

Tag* find_tag(Str tag) {
	for (int i=0; i<num_tags; i++) {
		if (!str_compare(tag, tags[i].tag_name, false)) {
			return tags+i;
		}
	}
	return 0;
}

Error decode_register(Str field, uint8_t* reg, char** last_char) {
	field = str_trim(field);
	if (field.str[0] != 'r' && field.str[0] != 'R') {
		return (Error) {
			.error = ERROR_INVALID_ADDRESSING_MODE,
			.position = field.str,
		};
	}
	if (field.len < 2) {
		return (Error) {
			.error = ERROR_INVALID_ADDRESSING_MODE,
			.position = field.str
		};
	}
	*last_char = field.str+2;
	uint8_t register_num = 0;
	if (isdigit(field.str[1])) {
		register_num = field.str[1] - '0';
	} else {
		switch (field.str[1]) {
			case 'A':
			case 'a':
				register_num = 10;
			break;
			case 'B':
			case 'b':
				register_num = 11;
			break;
			case 'C':
			case 'c':
				register_num = 12;
			break;
			case 'D':
			case 'd':
				register_num = 13;
			break;
			case 'E':
			case 'e':
				register_num = 14;
			break;
			case 'F':
			case 'f':
				register_num = 15;
			break;
			default:
				return (Error) {
					.error = ERROR_INVALID_ADDRESSING_MODE,
					.position = field.str + 1,
				};
		}
	}
	*reg = register_num;
	return (Error) {
		.error = ERROR_NONE,
		.position = 0,
	};
}

Error decode_number(Str field, uint32_t* n, char** last_char, State state) {
	field = str_trim(field);
	uint32_t number = 0;
	char* error = 0;
	Str num = {
		.str = field.str+2
	};
	if (field.str[0] == ':') {
		field.str++;
		field.len++;
		for (int i=0; i<field.len; i++) {
			if (isspace(field.str[i])) {
				field.len = i;
				break;
			}
		}
		Tag* tag = find_tag(field);
		if (!tag) {
			return (Error) {
				.error = ERROR_TAG_NOT_FOUND,
				.position = field.str
			};
		}
		*last_char = field.str+field.len-1;
		number = tag->addr;
		if (tag->addr == -2) {
			number = state.byte_number;
		}
		number+=state.origin;
	} else if (field.str[0] == '0') {
		switch (field.str[1]) {
			case 'x':
			case 'X': {
				for(int i=2; i<field.len; i++) {
					if (!isdigit(field.str[i]) && !(field.str[i] >= 'A' && field.str[i] <= 'F') && !(field.str[i] >= 'a' && field.str[i] <= 'f')) {
						break;
					} 
					*last_char = field.str+i;
					num.len++;
				}
				str_to_uint32_t_with_error(num, &number, 16, &error);
			} break;
			case 'b':
			case 'B': {
				for(int i=2; i<field.len; i++) {
					if (field.str[i] != '0' && field.str[i] != '1') {
						break;
					} 
					*last_char = field.str+i;
					num.len++;
				}
				str_to_uint32_t_with_error(num, &number, 2, &error);
			} break;
			case 'o':
			case 'O': {
				for(int i=2; i<field.len; i++) {
					if (field.str[i] >= '0' && field.str[i] <= '7') {
						break;
					} 
					*last_char = field.str+i;
					num.len++;
				}
				str_to_uint32_t_with_error(num, &number, 8, &error);
			} break;
			default: {
				if (!isdigit(field.str[1])) {
					return (Error) {
						.error = ERROR_INVALID_ADDRESSING_MODE,
						.position = field.str+1,
					};
				}
			}
		}
	} else {
		num.str-=2;
		for(int i=0; i<field.len; i++) {
			if (field.str[i] < '0' || field.str[i] > '9') {
				break;
			} 
			*last_char = field.str+i;
			num.len++;
		}
		str_to_uint32_t_with_error(num, &number, 10, &error);
	}
	if (error) {
		return (Error) {
			.error = ERROR_INVALID_NUMBER,
			.position = error,
		};
	}
	(*last_char)++;
	*n = number;
	return (Error) {
		.error = ERROR_NONE,
		.position = 0,
	};
}

Error decode_addressing_mode(Str f, uint8_t* addr_mode, uint8_t* reg, uint32_t* x, State state) {
	assert(addr_mode);
	assert(reg);
	assert(x);
	Str field = str_trim(f);
	char first_char = field.str[0];
	uint8_t current_addr_mode = 0;
	uint32_t X = 0;
	uint8_t register_number = 0;
	bool inParen = false;
	if (('0' <= first_char && first_char <= '9') || first_char == ':') {
		current_addr_mode = ADDRESSING_MODE_ABSOLUTE;
		Str temp = field;
		char* last_char = 0;
		bool inParen = false;
		Error err = decode_number(temp, &X, &last_char, state);
		if (err.error != ERROR_NONE) {
			return err;
		}
		if (!isspace(*last_char) && *last_char != '(') {
			return (Error) {
				.error = ERROR_UNKNOWN_SYMBOL,
				.position = last_char,
			};
		}
		if (*last_char == '(') {
			current_addr_mode = ADDRESSING_MODE_INDEXED;
			temp.str = last_char+1;
			temp.len = (field.len+field.str)-last_char;
			Error err = decode_register(temp, &register_number, &last_char);
			if (err.error != ERROR_NONE) {
				return err;
			}
			if (!isspace(*last_char) && *last_char != ')') {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = last_char,
				};
			}
			if (*last_char == ')') {
				inParen = false;
			}
			for (char* c = last_char + 1; last_char < field.str+field.len ; c++) {
				if (*c == ';') {
					break;
				}
				if (*c == ')') {
					if (!inParen) {
						return (Error) {
							.error = ERROR_UNKNOWN_SYMBOL,
							.position = c
						};
					}
					inParen = false;
				} else if (!isspace(*c)) {
					return (Error) {
						.error = ERROR_UNKNOWN_SYMBOL,
						.position = c
					};
				}
			}
		}
	} else if (first_char == 'r' || first_char == 'R') {
		current_addr_mode = ADDRESSING_MODE_REGISTER;
		char* last_char = 0;
		Error err = decode_register(field, &register_number, &last_char);
		if (!isspace(*last_char)) {
			return (Error) {
				.error = ERROR_UNKNOWN_SYMBOL,
				.position = last_char,
			};
		}
		if (err.error != ERROR_NONE) {
			return err;
		}
		for (char* c = last_char + 1; last_char < field.str+field.len ; c++) {
			if (!isspace(*c)) {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = c
				};
			}
		}
	} else if (first_char == '#') {
		current_addr_mode = ADDRESSING_MODE_IMMEDIATE;
		Str temp = field;
		temp.str++;
		temp.len--;
		char* last_char = 0;
		Error err = decode_number(temp, &X, &last_char, state);
		if (err.error != ERROR_NONE) {
			return err;
		}
		if (!isspace(*last_char)) {
			return (Error) {
				.error = ERROR_UNKNOWN_SYMBOL,
				.position = last_char,
			};
		}
		for (char* c = last_char + 1; last_char < field.str+field.len ; c++) {
			if (!isspace(*c)) {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = c
				};
			}
		}
	} else if (first_char == '(') {
		current_addr_mode = ADDRESSING_MODE_INDIRECT;
		inParen = true;
		char* last_char = 0;
		if (field.str[1] == 'r' || field.str[1] == 'R') {
			current_addr_mode = ADDRESSING_MODE_REGISTER;
			Str temp = field;
			temp.str++;
			Error err = decode_register(temp, &register_number, &last_char);
			if (!isspace(*last_char) && *last_char != ')') {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = last_char,
				};
			}
			if (*last_char == ')') {
				inParen = false;
			}
			if (err.error != ERROR_NONE) {
				return err;
			}
		} else if (isdigit(field.str[1])) {
			Str temp = field;
			temp.str++;
			Error err = decode_number(temp, &X, &last_char, state);
			if (err.error != ERROR_NONE) {
				return err;
			}
			if (!isspace(*last_char) && *last_char != ')') {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = last_char,
				};
			}
			if (*last_char == ')') {
				inParen = false;
			}
		} else {
			return (Error) {
				.error = ERROR_UNKNOWN_SYMBOL,
				.position = field.str + 1,
			};
		}
		for (char* c = last_char+1; c<field.str+field.len; c++) {
			if (*c == ')') {
				if(!inParen) {
					return (Error) {
						.error = ERROR_UNKNOWN_SYMBOL,
						.position = c,
					};
				}
				inParen = false;
			} else if(!isspace(*c)) {
				return (Error) {
					.error = ERROR_UNKNOWN_SYMBOL,
					.position = c,
				};
			}
		}
	} else {
		return (Error) {
			.error = ERROR_INVALID_ADDRESSING_MODE,
			.position = field.str+0,
		};
	}
	*x = X;
	*addr_mode = current_addr_mode;
	*reg = register_number;
	return (Error) {
		.error = ERROR_NONE,
		.position = 0,
	};
}

Error decode_arguments(Str arguments, Instruction instruction, uint8_t* registers, uint32_t* x, uint8_t* addressing_mode, State state) {
	size_t num_args = 0;
	uint8_t addr_mode = 0;
	uint8_t dest_reg = 0;
	uint8_t src_reg = 0;
	uint32_t X = 0;
	if (instruction.argument_count == 0) {
		for (int i=0; i<arguments.len; i++) {
			if (!isspace(arguments.str[i])) {
				return (Error) {
					.error = ERROR_TOO_MANY_ARGS,
					.position = arguments.str+i,
				};
			}
		}
		*x = 0;
		*registers = OPCODE_REGISTERS(0, 0);
	} else if (instruction.argument_count == 1) {
		bool wasSpace = true;
		for (int i=0; i<arguments.len; i++) {
			if (isspace(arguments.str[i])) {
				wasSpace = true;
				continue;
			}
			if (wasSpace) {
				if (num_args == 0) {
					Str s = {
						.str = arguments.str+i,
						.len = arguments.len-i,
					};
					Error err = decode_addressing_mode(s, &addr_mode, &src_reg, &X, state);
					if (err.error != ERROR_NONE) {
						return err;
					}
				} else {
					return (Error) {
						.error = ERROR_TOO_MANY_ARGS,
						.position = arguments.str+i,
					};
				}
				num_args++;
			}
			wasSpace = false;
		}
	} else if (instruction.argument_count == 2) {
		bool wasSpace = true;
		for (int i=0; i<arguments.len; i++) {
			if (isspace(arguments.str[i])) {
				wasSpace = true;
				continue;
			}
			char* c = 0;
			if (wasSpace) {
				if (num_args == 0) {
					Str s = {
						.str = arguments.str+i,
						.len = arguments.len-i,
					};
					Error err = decode_register(s, &dest_reg, &c);
					if (err.error != ERROR_NONE) {
						return err;
					}
				} else if (num_args == 1) {
					Str s = {
						.str = arguments.str+i,
						.len = arguments.len-i,
					};
					Error err;
					if (instruction.enable_addressing_mode) {
						err = decode_addressing_mode(s, &addr_mode, &src_reg, &X, state);
					} else {
						err = decode_register(s, &src_reg, &c);
					}
					if (err.error != ERROR_NONE) {
						return err;
					}
				} else {
					return (Error) {
						.error = ERROR_TOO_MANY_ARGS,
						.position = arguments.str+i,
					};
				}
				num_args++;
			}
			wasSpace = false;
		}
	}
	*registers = OPCODE_REGISTERS(dest_reg, src_reg);
	*x = X;
	*addressing_mode = addr_mode;
	return (Error) {
		.error = ERROR_NONE,
		.position = 0,
	};
}

void error_print(Error error, Str line, size_t line_number) {
	str_print(line);
	printf("\n");
	if (error.position) {
		for (char* c = line.str; c < error.position; c++) {
			printf(" ");
		}
		printf("^\n");
	}
	printf("Error at %ld: %s\n", line_number, error_type_strings[error.error]);
}

void find_tags(Str file_content, State* state, size_t max_data_len) {
	int i=0;
	size_t current_byte_num = 0;
	for (Str line = str_split_by_char(file_content, '\n', 0); line.len; line = str_split_by_char(file_content, '\n', ++i)) {
		printf("current byte num: %ld, line number: %d\n", current_byte_num, i+1);
		Str trimmed_line = str_trim(line);
		Str comments_removed = str_trim(str_split_by_char(trimmed_line, ';', 0));
		Str command = str_trim(str_split_by_char(comments_removed, ' ', 0));
		if (!command.len) {
			continue;
		}
		if (command.str[0] == ':') {
			command.str++;
			command.len--;
			char* str = ((char*)state->arena->start)+state->arena->curr;
			printf("%p, %ld\n", str, state->arena->curr);
			for (int i=0; i<command.len; i++) {
				state->arena->start[state->arena->curr++] = command.str[i];
			}
			tags[num_tags++] = (Tag) {
				.addr = current_byte_num,
				.tag_name = (Str) {
					.str = str,
					.len = command.len
				}
			};
			for (int i=0; i<num_tags; i++) {
				tag_print(tags[num_tags-1]);
			}
			continue;
		}
		if (!str_compare(command, STR_FROM_CSTRING_ARR(".data"), false)) {
			Str data_str = str_trim(str_split_by_char(comments_removed, ' ', 1));
			int i=0;
			for(i=1; i<max_data_len && data_str.len; i++) {
				data_str = str_trim(str_split_by_char(comments_removed, ' ', i+1));
			}
			current_byte_num+=i;
			continue;
		}
		Instruction* instruction = find_instruction(command);
		if (instruction == 0) {
			continue;
		}
		Str args = {
			.len = comments_removed.len-command.len,
			.str = command.str+command.len,
		};
		args = str_trim(args);
		if (instruction->argument_count == 0) {
			current_byte_num++;
		} else if (instruction->argument_count == 1 && instruction->enable_addressing_mode) {
			if (args.str[0] == ':' || args.str[0] == '#' || (args.str[0] <= '9' && args.str[0] >= '0') || (args.str[0] == '(' && (args.str[1] <= '9' && args.str[1] >= '0'))) {
				current_byte_num++;
			}
			current_byte_num++;
		} else if (instruction->argument_count == 2 && instruction->enable_addressing_mode) {
			args = str_trim(str_split_by_char(args, ' ', 1));
			if (args.str[0] == ':' || args.str[0] == '#' || (args.str[0] <= '9' && args.str[0] >= '0') || (args.str[0] == '(' && (args.str[1] <= '9' && args.str[1] >= '0'))) {
				current_byte_num++;
			}
			current_byte_num++;
		} else {
			current_byte_num++;
		}
	}
}

size_t assemble_1_line(Str line, uint32_t* data, size_t max_data_len, State* state) {
	if (max_data_len < 1) {
		return -1;
	}
	size_t line_number = state->line_number;
	Str trimmed_line = str_trim(line);
	Str comments_removed = str_trim(str_split_by_char(trimmed_line, ';', 0));
	Str command = str_trim(str_split_by_char(comments_removed, ' ', 0));
	if (!command.len) {
		return 0;
	}
	if (command.str[0] == '.') {
		command.str++;
		command.len--;
		if (!str_compare(command, STR_FROM_CSTRING_ARR("origin"), false)) {
			Str number = str_trim(str_split_by_char(comments_removed, ' ', 1));
			uint32_t n = 0;
			char* c = 0;
			decode_number(number, &n, &c, *state);
			state->origin = n;
			return 0;
		} else if (!str_compare(command, STR_FROM_CSTRING_ARR("data"), false)) {
			Str data_str = str_trim(str_split_by_char(comments_removed, ' ', 1));
			char* c;
			int i=0;
			for(i=1; i<max_data_len && data_str.len; i++) {
				Error err = decode_number(data_str, data+i, &c, *state);
				if (err.error != ERROR_NONE) {
					error_print(err, line, line_number);
					return -1;
				}
				data_str = str_trim(str_split_by_char(comments_removed, ' ', i+1));
			}
			return i;
		}
		return -1;
	} else if (command.str[0] == ':') {
	// 	command.str++;
	// 	command.len--;
	// 	char* str = ((char*)state->arena->start)+state->arena->curr;
	// 	printf("%p, %ld\n", str, state->arena->curr);
	// 	for (int i=0; i<command.len; i++) {
	// 		state->arena->start[state->arena->curr++] = command.str[i];
	// 	}
	// 	tags[num_tags++] = (Tag) {
	// 		.addr = state->byte_number,
	// 		.tag_name = (Str) {
	// 			.str = str,
	// 			.len = command.len
	// 		}
	// 	};
	// 	for (int i=0; i<num_tags; i++) {
	// 		tag_print(tags[num_tags-1]);
	// 	}
		return 0;
	} else {
		Instruction* instruction = find_instruction(command);
		if (instruction == 0) {
			Error err = {
				.error = ERROR_INSTRUCTION_NOT_FOUND,
				.position = line.str,
			};
			error_print(err, line, line_number);
			return -1;
		}
		Str args = {
			.len = comments_removed.len-command.len,
			.str = command.str+command.len,
		};
		args = str_trim(args);
		uint8_t reg = 0;
		uint32_t x = 0;
		uint8_t addr_mode = 0;
		Error err = decode_arguments(args, *instruction, &reg, &x, &addr_mode, *state);
		if (err.error != ERROR_NONE) {
			error_print(err, line, line_number);
			return -1;
		}
		switch (addr_mode) {
			case 1:
			case 2:
			case 3:
			case 5:
				data[0] = (reg << 8) | (instruction->opcode | addr_mode);
				data[1] = x;
			return 2;
			default:
				if (instruction->enable_addressing_mode) {
					data[0] = (reg << 8) | (instruction->opcode | addr_mode);
				} else {
					data[0] = (reg << 8) | (instruction->opcode);
				}
			return 1;
		}
	}
}
