#ifndef ARENA__H
#define ARENA__H

#include <stddef.h>

typedef struct {
	char* start;
	size_t len;
	size_t curr;
} Arena;

Arena arena_create(size_t len);
void arena_free(Arena* arena);

#endif
