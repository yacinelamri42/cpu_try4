#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "arena.h"
#include "opcode.h"
#include "panic.h"
#include "str.h"
#include "list.h"

#define DATA_MAX_LEN 256

int main(int argc, char* argv[]) {
	if (argc < 3) {
		printf("Usage: %s <input> <output>\n", argv[0]);
		return 1;
	}
	char* input_filename = argv[1];
	char* output_filename = argv[2];
	FILE* output_file = fopen(output_filename, "wb");
	FILE* input_file = fopen(input_filename, "r");
	Arena arena = arena_create(64*1024);
	if (!arena.start) {
		return 1;
	}
	State state = {
		.origin = 0,
		.byte_number = 0,
		.line_number = 0,
		.arena = &arena
	};
	fseek(input_file, 0, SEEK_END);
	size_t input_file_size = ftell(input_file);
	Str file_content = {
		.str = calloc(input_file_size, 1),
		.len = input_file_size
	};
	if (!file_content.str) {
		PANIC("???? error");
	}
	fseek(input_file, 0, SEEK_SET);
	fread(file_content.str, file_content.len, 1, input_file);
	find_tags(file_content, &state, DATA_MAX_LEN);
	for (Str l = str_split_by_char(file_content, '\n', 0); l.len; l = str_split_by_char(file_content, '\n', ++state.line_number)) {
		uint32_t data[DATA_MAX_LEN] = {0};
		size_t len = assemble_1_line(l, data, DATA_MAX_LEN, &state);
		if (len == -1) {
			return 1;
		}
		state.byte_number+=len;
		fwrite(data, len, 4, output_file);
		for (int i=0; i<len; i++) {
			printf("%X ", data[i]);
		}
		printf("\n");
	}
	fclose(input_file);
	fclose(output_file);
	// FILE* f = fopen("debug", "w");
	// fwrite(arena.start, arena.len, 1, f);
	// fclose(f);
	free(file_content.str);
	arena_free(&arena);
	return 0;
}
