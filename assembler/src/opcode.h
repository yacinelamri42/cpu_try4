#ifndef OPCODE__H
#define OPCODE__H

#include "str.h"

#define ADDRESSING_MODE_REGISTER 	  0
#define ADDRESSING_MODE_IMMEDIATE 	  1
#define ADDRESSING_MODE_ABSOLUTE 	  2
#define ADDRESSING_MODE_INDIRECT 	  3
#define ADDRESSING_MODE_REGISTER_INDIRECT 4
#define ADDRESSING_MODE_INDEXED 	  5

#define INT_ENABLE_INT_MODE		  8
#define INT_PAUSE_INT_MODE		  7
#define INT_IRET_MODE			  6
#define MOV_STORE_MODE 			  8

#define OPCODE_REGISTERS(dest, src) (((dest) & 0xf) << 4 | ((src) & 0xf))

#define OPCODE_NOP 			  0
#define OPCODE_MOV(addressing_mode)	0x10 | (addressing_mode)
#define OPCODE_ADD(addressing_mode)	0x20 | (addressing_mode)
#define OPCODE_SUB(addressing_mode)	0x30 | (addressing_mode)
#define OPCODE_AND(addressing_mode)	0x40 | (addressing_mode)
#define OPCODE_OR(addressing_mode)	0x50 | (addressing_mode)
#define OPCODE_NOT(addressing_mode)	0x60 | (addressing_mode)
#define OPCODE_SHIFTL(addressing_mode)	0x70 | (addressing_mode)
#define OPCODE_SHIFTR(addressing_mode)	0x80 | (addressing_mode)
#define OPCODE_JMP(addressing_mode)	0x90 | (addressing_mode)
#define OPCODE_JZ(addressing_mode)	0xA0 | (addressing_mode)
#define OPCODE_JC(addressing_mode)	0xB0 | (addressing_mode)
#define OPCODE_IN(addressing_mode)	0xC0 | (addressing_mode)
#define OPCODE_OUT(addressing_mode)	0xD0 | (addressing_mode)
#define OPCODE_INT(addressing_mode)	0xE0 | (addressing_mode)

#include "arena.h"

typedef struct {
	size_t line_number;
	size_t origin;
	size_t byte_number;
	Arena* arena;
} State;

void find_tags(Str file_content, State* state, size_t max_data_len);
size_t assemble_1_line(Str line, uint32_t* data, size_t max_data_len, State* state);

#endif
