#include "arena.h"
#include <stdlib.h>

Arena arena_create(size_t len) {
	return (Arena) {
		.start = calloc(len, 1),
		.len = len,
		.curr = 0
	};
}

void arena_free(Arena* arena) {
	free(arena->start);
	arena->curr = 0;
	arena->len = 0;
}
