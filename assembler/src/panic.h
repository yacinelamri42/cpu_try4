#ifndef PANIC__H
#define PANIC__H

#include <stdlib.h>
#include <stdio.h>

#define PANIC(message, ...) \
	fprintf(stderr, "Panic at %s:%d\n", __FILE__, __LINE__); \
	fprintf(stderr, message, ##__VA_ARGS__);\
	exit(1)


#endif
