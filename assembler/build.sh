#!/bin/bash

PROJECT_ROOT=$(dirname $0)

gcc -Wall -O2 -fsanitize=address -g $PROJECT_ROOT/src/*.c -o $PROJECT_ROOT/bin/assembler
