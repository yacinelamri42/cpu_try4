#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


typedef struct {
	uint32_t len;
	uint32_t crc;
	char chunk_type[4];
	uint8_t* data;
} Chunk;

typedef enum {
	CHUNK_ERROR_NONE,
	CHUNK_ERROR_IO,
	CHUNK_ERROR_MALLOC,
	CHUNK_ERROR_WRONG_CRC,
} ChunkError;

// ---------------- CRC CODE EXAMPLE(current me too dumb to figure out CRC)
/* Table of CRCs of all 8-bit messages. */
   unsigned long crc_table[256];
   
   /* Flag: has the table been computed? Initially false. */
   int crc_table_computed = 0;
   
   /* Make the table for a fast CRC. */
   void make_crc_table(void)
   {
     unsigned long c;
     int n, k;
   
     for (n = 0; n < 256; n++) {
       c = (unsigned long) n;
       for (k = 0; k < 8; k++) {
         if (c & 1)
           c = 0xedb88320L ^ (c >> 1);
         else
           c = c >> 1;
       }
       crc_table[n] = c;
     }
     crc_table_computed = 1;
   }
   
   /* Update a running CRC with the bytes buf[0..len-1]--the CRC
      should be initialized to all 1's, and the transmitted value
      is the 1's complement of the final running CRC (see the
      crc() routine below)). */
   
   unsigned long update_crc(unsigned long crc, unsigned char *buf,
                            int len)
   {
     unsigned long c = crc;
     int n;
   
     if (!crc_table_computed)
       make_crc_table();
     for (n = 0; n < len; n++) {
       c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
     }
     return c;
   }
   
   /* Return the CRC of the bytes buf[0..len-1]. */
   unsigned long crc(unsigned char *buf, int len)
   {
     return update_crc(0xffffffffL, buf, len) ^ 0xffffffffL;
   }
// ------------------------------------------------------------

bool chunkTypeCompare(const char* chunk_type, const char* other) {
	for (int i=0; i<4; i++) {
		char a = chunk_type[i] | 32;
		char b = other[i] | 32;
		if(a != b) {
			return false;
		}
	}
	return true;
}

ChunkError readChunk(FILE* file, Chunk* newChunk) {
	Chunk c = {0};
	int err1 = fread(&c.len, sizeof(c.len), 1, file);
	int err2 = fread(&c.chunk_type, sizeof(c.chunk_type), 1, file);
	uint8_t* data = calloc(c.len, 1);
	uint8_t* temp = calloc(4+c.len, 1);
	if (!data || !temp) {
		free(temp);
		free(data);
		return CHUNK_ERROR_MALLOC;
	}
	memcpy(temp, c.chunk_type, sizeof(c.chunk_type));
	int err3 = fread(data, c.len, 1, file);
	memcpy(temp+sizeof(c.chunk_type), data, sizeof(c.len));
	int err4 = fread(&c.crc, sizeof(c.crc), 1, file);
	if (err1 < 4 || err2 < 4 || err3 < c.len || err4 < 4) {
		free(temp);
		free(data);
		return CHUNK_ERROR_IO;
	}
	uint32_t crc_computed = crc(temp, 4+c.len);
	free(temp);
	if (crc_computed != c.crc) {
		free(data);
		return CHUNK_ERROR_WRONG_CRC;
	}
	*newChunk = c;
	return CHUNK_ERROR_NONE;
}

const uint8_t png_header[] = {137, 80, 78, 71, 13, 10, 26, 10};

int main(int argc, char* argv[]) {
	if (argc != 3) {
		fprintf(stderr, "USAGE: %s image.png output", argv[0]);
		return 1;
	}
	FILE* png = fopen(argv[1], "r");
	if (!png) {
		fprintf(stderr, "Could not open file: %s\n", argv[1]);
		return -1;
	}
	uint8_t header[8] = {0};
	if(fread(header, 8, 1, png) == -1) {
		fprintf(stderr, "Couldnt read header\n");
		fclose(png);
		return 1;
	}
	for (int i=0; i<sizeof(png_header); i++) {
		if (header[i] != png_header[i]) {
			fprintf(stderr, "Wrong header\n");
			fclose(png);
			return 1;
		}
	}
	Chunk c = {0};
	
	for (int i=0; readChunk(png, &c) == CHUNK_ERROR_NONE; i++) {
		if(!chunkTypeCompare(c.chunk_type, "IHDR") && !i) {
			fprintf(stderr, "IHDR not found\n");
			return 1;
		}
		if (chunkTypeCompare(c.chunk_type, "IEND")) {
		
		}
	}
	fclose(png);
	return 0;
}

