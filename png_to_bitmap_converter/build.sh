#!/bin/bash

PROGRAM_ROOT=$(dirname $0)

gcc -Wall -g $PROGRAM_ROOT/src/*.c -o $PROGRAM_ROOT/bin/png_to_bitmap_converter
