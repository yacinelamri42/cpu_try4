#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("needs a input\n");
		return 1;
	}
	int client_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (client_fd == -1) {
		return 1;
	}
	struct sockaddr_in addr = {
		.sin_family = AF_INET,
		.sin_addr.s_addr = inet_addr("127.0.0.1"),
		.sin_port = htons(4412),
	};
	if(connect(client_fd, (struct sockaddr*)&addr, sizeof(addr))) {
		return 1;
	}
	for (int i=1; i<argc; i++) {
		int fd = open(argv[i], O_RDONLY);
		uint8_t data[1024] = {0};
		int len = 0;
		while ((len = read(fd, data, 1024)) > 0) {
			if (len == -1) {
				perror("error in reading");
				return 1;
			}
			uint8_t a = 0;
			for (int j=0; j<len; j++) {
				send(client_fd, data+j, 1, 0);
			}
			if (len == 1024) {
				recv(client_fd, &a, 1, 0);
			}
		}
		char c = 0;
		send(client_fd, &c, 1, 0);
		close(fd);
	}
	close(client_fd);
	return 0;
}
